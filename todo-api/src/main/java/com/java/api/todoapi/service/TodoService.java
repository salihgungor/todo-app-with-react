package com.java.api.todoapi.service;

import com.java.api.todoapi.dto.Todo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class TodoService {

    private static List<Todo> todos = new ArrayList<>();
    private static int idCounter = 0;

    static {
        todos.add(new Todo(++idCounter, "sekvann", "sekvanın todosundan biri", new Date(), false));
        todos.add(new Todo(++idCounter, "sekvan", "sekvan todosundan biri", new Date(), true));
        todos.add(new Todo(++idCounter, "emir", "emir todosundan biri", new Date(), false));
        todos.add(new Todo(++idCounter, "sacit", "sacit todosundan biri", new Date(), false));
    }

    public List<Todo> findAll() {
        return todos;
    }

    public Todo removeById(int id) {
        Todo todo = findById(id);
        if (Objects.nonNull(todo)) {
            todos.remove(todo);
        }
        return null;
    }

    public Todo addTodo(Todo todo) {
        todos.add(new Todo(++idCounter, todo.getName(), todo.getDescription(), todo.getTargetDate(), true));
        return todo;
    }

    public Todo updateTodo(Todo todo) {
        Todo gettedTodo = findById(todo.getId());
        if (Objects.nonNull(gettedTodo)) {
            return todos.stream()
                    .filter(t -> Objects.equals(t.getId(), todo.getId()))
                    .peek(t -> {
                        t.setName(todo.getName());
                        t.setDescription(todo.getDescription());
                    })
                    .findFirst().orElse(null);
        }
        return null;
    }

    public Todo findById(int id) {
        return todos.stream().filter(todo -> todo.getId() == id).findFirst().orElse(null);
    }

}
