package com.java.api.todoapi.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Todo {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String description;
    private Date targetDate;
    private boolean isCompleted;

    public Todo() {}

    public Todo(int id, String name, String description, Date targetDate, boolean isCompleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.targetDate = targetDate;
        this.isCompleted = isCompleted;
    }

}
