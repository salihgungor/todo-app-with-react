package com.java.api.todoapi.dto.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class AuthenticationBean {

    private String message;

    public AuthenticationBean(String message) {
        this.message = message;
    }

}
