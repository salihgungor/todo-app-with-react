package com.java.api.todoapi.repository;

import com.java.api.todoapi.dto.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TodoRepository extends JpaRepository<com.java.api.todoapi.entity.Todo, Integer> {

    List<Todo> findByName(String name);

}
