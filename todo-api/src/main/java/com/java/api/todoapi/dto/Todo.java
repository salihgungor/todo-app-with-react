package com.java.api.todoapi.dto;

import lombok.Data;

import java.util.Date;

@Data
public class Todo {

    private int id;
    private String name;
    private String description;
    private Date targetDate;
    private boolean isCompleted;

    public Todo(int id, String name, String description, Date targetDate, boolean isCompleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.targetDate = targetDate;
        this.isCompleted = isCompleted;
    }
}
