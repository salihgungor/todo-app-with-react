package com.java.api.todoapi.controller;

import com.java.api.todoapi.entity.Todo;
import com.java.api.todoapi.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@CrossOrigin
@RestController
public class TodoJpaResource {

   @Autowired
   private TodoRepository todoRepository;

    @GetMapping("/jpa/users/{username}/todos")
    public List<Todo> getAllTodos(@PathVariable String username) {
        return todoRepository.findAll();
    }

    @GetMapping("/jpa/users/{username}/todos/{id}")
    public Todo getTodo(@PathVariable String username, @PathVariable int id) {
        Todo todo = todoRepository.findById(id).orElse(null);
        if (Objects.nonNull(todo))
            return todo;
        return null;
    }

    @DeleteMapping("/jpa/users/{username}/todos/{id}")
    public void removeTodo(@PathVariable String username, @PathVariable int id) {
        todoRepository.deleteById(id);
    }

    @PutMapping("/jpa/users/{username}/todos")
    public Todo updateTodo(@RequestBody Todo todo, @PathVariable String username) {
        return todoRepository.save(todo);
    }

    @PostMapping("/jpa/users/{username}/todos")
    public Todo addTodo(@RequestBody Todo todo, @PathVariable String username) {
        return todoRepository.save(todo);
    }

}
