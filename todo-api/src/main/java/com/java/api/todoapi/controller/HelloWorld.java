package com.java.api.todoapi.controller;

import com.java.api.todoapi.dto.HelloWorldBean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class HelloWorld {

    @GetMapping("hello-world")
    public String getHelloMessage() {
        return "Hello world";
    }

    @GetMapping("hello-world-bean")
    public HelloWorldBean getHelloBeanMessage() {
        return new HelloWorldBean("helloooooo");
    }

    @GetMapping("hello-world/path-variable/{name}")
    public HelloWorldBean getHelloMessageWithPathVariable(@PathVariable String name) {
        return new HelloWorldBean("helloooooo " + name);
    }

    @GetMapping("hello-world/throw-exception")
    public HelloWorldBean throwException() {
        throw new RuntimeException("Something went wrong.");
    }

}
