package com.java.api.todoapi.controller;

import com.java.api.todoapi.dto.jwt.AuthenticationBean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class BasicAuthenticationController {

    @GetMapping("basicauth")
    public AuthenticationBean basicAuth() {
        return new AuthenticationBean("You are authenticated.");
    }

}
