package com.java.api.todoapi.controller;

import com.java.api.todoapi.dto.Todo;
import com.java.api.todoapi.service.TodoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@CrossOrigin
@RestController
public class TodoResource {

    private final TodoService todoService;

    public TodoResource(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/users/{username}/todos")
    public List<Todo> getAllTodos(@PathVariable String username) {
        // Thread.sleep(3000);
        return todoService.findAll();
    }

    @GetMapping("/users/{username}/todos/{id}")
    public Todo getTodo(@PathVariable String username, @PathVariable int id) {
        Todo todo = todoService.findById(id);
        if (Objects.nonNull(todo))
            return todo;
        return null;
    }

    @DeleteMapping("/users/{username}/todos/{id}")
    public Todo removeTodo(@PathVariable String username, @PathVariable int id) {
        Todo todo = todoService.removeById(id);
        if (Objects.nonNull(todo))
            return todo;
        return null;
    }

    @PutMapping("/users/{username}/todos")
    public Todo updateTodo(@RequestBody Todo todo, @PathVariable String username) {
        Todo gettedTodo = todoService.updateTodo(todo);
        if (Objects.nonNull(gettedTodo))
            return gettedTodo;
        return null;
    }

    @PostMapping("/users/{username}/todos")
    public Todo addTodo(@RequestBody Todo todo, @PathVariable String username) {
        Todo gettedTodo = todoService.addTodo(todo);
        if (Objects.nonNull(gettedTodo))
            return gettedTodo;
        return null;
    }

}
