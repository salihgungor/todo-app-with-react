import axios from "axios";
import {API_URL} from "../Constants";

export default class HelloWorldService {

    static executeHelloWorldService() {
        return axios.get(`${API_URL}/hello-world`);
    }

    static executeHelloWorldBeanService() {
        return axios.get(`${API_URL}/hello-world-bean`);
    }

    static executeHelloWorldPathVariableService(name) {
        return axios.get(`${API_URL}/hello-world/path-variable/${name}`);
    }

    static throwException() {
        return axios.get(`${API_URL}/hello-world/throw-exception`);
    }

    // test promise method
    static test(param) {
        return new Promise((resolve, reject) => {
            resolve(param);
        });
    }

}

