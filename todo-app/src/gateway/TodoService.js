import axios from "axios";
import {API_URL, JPA_API_URL} from "../Constants";

export default class TodoService {

    static getAllTodos(name) {
        return axios.get(`${JPA_API_URL}/users/${name}/todos`);
    }

    static getTodo(id, name) {
        return axios.get(`${JPA_API_URL}/users/${name}/todos/${id}`);
    }

    static removeTodo(id, name) {
        return axios.delete(`${JPA_API_URL}/users/${name}/todos/${id}`);
    }

    static addTodo(id, name, todo) {
        return axios.post(`${JPA_API_URL}/users/${name}/todos`, todo);
    }

    static updateTodo(id, name, todo) {
        return axios.put(`${JPA_API_URL}/users/${name}/todos`, todo);
    }

}