import React, {Component} from "react";
import TodoService from "../../gateway/TodoService";
import AuthenticationService from "../authentication/AuthenticationService";
import moment from "moment";

export default class ListTodosComponent extends Component {

    constructor(props) {
        console.log('constructor');
        super(props);
        this.state = {
            todos: [],
            message: null
        }

        this.removeTodo = this.removeTodo.bind(this);
        this.addTodo = this.addTodo.bind(this);
        this.updateTodo = this.updateTodo.bind(this);
        this.refreshTodos = this.refreshTodos.bind(this);
    }

    render() {
        console.log('render');
        return (
            <div className="list-todos">
                <h1>List Todos</h1>
                { this.state.message && <div className="alert alert-success">{this.state.message}</div> }
                <div className="container">
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th>Is Completed</th>
                            <th>Target Date</th>
                            <th>Delete</th>
                            <th>Update</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.todos.map(
                                todo =>
                                    <tr key={todo.id}>
                                        <td>{todo.description}</td>
                                        <td>{todo.completed.toString()}</td>
                                        <td>{moment(todo.targetDate).format('YYYY-MM-DD')}</td>
                                        <td><button className="btn btn-danger" onClick={() => this.removeTodo(todo.id)}>Remove</button></td>
                                        <td><button className="btn btn-warning" onClick={() => this.updateTodo(todo.id)}>Update</button></td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                    <div className="row">
                        <button className="btn btn-success" onClick={this.addTodo}>Add</button>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        console.log('componentDidMount');
        this.refreshTodos();
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('shouldComponentUpdate');
        console.log('nextProps: ' + nextProps);
        console.log('nextState: ' + nextState);
        return true;
    }

    removeTodo(id) {
        let username = AuthenticationService.getUsername();
        TodoService.removeTodo(id, username)
            .then(response => {
                this.setState(
                    {
                        message: `Delete of todo ${id} Successful`
                    }
                )
                this.refreshTodos()
            })
    }

    addTodo() {
        this.props.history.push(`/todos/-1`);
    }

    updateTodo(id) {
        this.props.history.push(`/todos/${id}`);
    }

    refreshTodos() {
        let username = AuthenticationService.getUsername();
        TodoService.getAllTodos(username)
            .then(response => {
                this.setState(
                    {
                        todos: response.data
                    }
                )
            });
    }

}
