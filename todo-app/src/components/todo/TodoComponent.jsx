import {Component} from "react";
import moment from "moment";
import {Formik, Form, Field, ErrorMessage} from 'formik'
import TodoService from "../../gateway/TodoService";
import AuthenticationService from "../authentication/AuthenticationService";

export default class TodoComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            description: '',
            targetDate: moment(new Date()).format('YYYY-MM-DD')
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.validate = this.validate.bind(this);
    }

    render() {
        let description = this.state.description;
        let targetDate = this.state.targetDate;

        return (
            <div className="todo-component">
                <h1>Todo</h1>
                <div className="container">
                    <Formik
                        initialValues = {{
                            description,
                            targetDate
                        }}
                        onSubmit={this.onSubmit}
                        validate={this.validate}
                        validateOnChange={false}
                        validateOnBlur={false}
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="description" component="div" className="alert alert-warning"/>
                                    <ErrorMessage name="targetDate" component="div" className="alert alert-warning"/>
                                    <fieldset className="form-group">
                                        <label htmlFor="">Description</label>
                                        <Field className="form-control" type="text" name="description" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label htmlFor="">Target Date</label>
                                        <Field className="form-control" type="date" name="targetDate" />
                                    </fieldset>
                                    <button type="submit" className="btn btn-success">Save</button>
                                </Form>
                            )
                        }
                    </Formik>
                </div>
            </div>
        );
    }

    onSubmit(values) {
        let todo = {
            id: this.state.id,
            description: values.description,
            targetDate: values.targetDate
        }
        let username = AuthenticationService.getUsername();

        if (this.state.id === '-1') {
            TodoService.addTodo(this.state.id, username, todo)
                .then(response => {
                    // routing
                    this.props.history.push('/todos');
                })
        } else {
            TodoService.updateTodo(this.state.id, username, todo)
                .then(response => {
                    // routing
                    this.props.history.push('/todos');
                })
        }

    }

    validate(values) {
        let errors = {}
        if (!values.description) {
            errors.description = 'Enter a description'
        } else if (values.description.length < 5) {
            errors.description = 'Enter atleast 5 characters in description'
        }

        if (!moment(values.targetDate).isValid()) {
            errors.targetDate = 'Enter valid target date'
        }
        return errors;
    }

    componentDidMount() {
        if (this.state.id === '-1') {
            return;
        }

        let username = AuthenticationService.getUsername();
        TodoService.getTodo(this.state.id, username)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    description: response.data.description,
                    targetDate: moment(response.data.targetDate).format('YYYY-MM-DD'),
                })
                console.log('get todo çalıştı.')
            })
    }

}