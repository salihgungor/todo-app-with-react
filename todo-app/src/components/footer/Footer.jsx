import React, {Component} from "react";

export default class FooterComponent extends Component {

    render() {
        return (
            <footer className="footer">
                <span className="text-muted">
                    All rights reserved by Sekvan Güngör - 2021
                </span>
            </footer>
        );
    }

}
