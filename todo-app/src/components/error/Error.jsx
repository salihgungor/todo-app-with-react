import React, {Component} from "react";

export default class ErrorComponent extends Component {

    render() {
        return (
            <div className="error">
                <h1>
                    An Error Occured. I dont know what to do! Contact support.
                </h1>
            </div>
        );
    }

}
