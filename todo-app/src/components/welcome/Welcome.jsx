import React, {Component} from "react";
import {Link} from "react-router-dom";
import HelloWorldService from "../../gateway/HelloWorldService";

export default class WelcomeComponent extends Component {

    constructor(props) {
        super(props);
        this.retrieveWelcomeMessage = this.retrieveWelcomeMessage.bind(this);
        this.state = {
            welcomeMessage: ''
        }
    }

    render() {
        return (
            <div className="welcome">
                <h1>Welcome {this.props.match.params.name}</h1>
                <div className="container">
                    <h6>You can manage your todos link <Link to="/todos">here.</Link></h6>
                </div>
                <div className="container">
                    Click here to get a customized welcome message.
                    <button className="btn btn-success" onClick={this.retrieveWelcomeMessage}>Get Welcome Message</button>
                </div>
                <div className="container">
                    { this.state.welcomeMessage }
                </div>
            </div>
        )
    }

    retrieveWelcomeMessage() {
        HelloWorldService.throwException()
            .then(response => {
                console.log(response);
                this.setState({
                    welcomeMessage: response.data.message
                })
            })
            .catch(error => {
                console.log('error: ' + error);

                let errorMessage = '';
                if (error.message)
                    errorMessage += error.message;

                if (error.response && error.response.data)
                    errorMessage += error.message;

                this.setState({
                    welcomeMessage: errorMessage
                })
            });

/*        HelloWorldService.test('testtttttttt')
            .then(response => console.log(response));*/
    }

/*    async retrieveWelcomeMessage() {
        let data;
        try {
            data = await HelloWorldService.executeHelloWorldService();

        } catch (e) {
            throw e;
        }
        console.log(data);

    }*/

}
