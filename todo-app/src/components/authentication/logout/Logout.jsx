import React, {Component} from "react";

export default class LogoutComponent extends Component {

    render() {
        return (
            <div>
                <h2>You are logged out.</h2>
                <div className="container">
                    Thank you for using our application.
                </div>
            </div>
        );
    }

}
