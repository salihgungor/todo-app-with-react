import React, {Component} from "react";
import AuthenticationService from "../AuthenticationService";

export default class LoginComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.loginClicked = this.loginClicked.bind(this);
    }

    render() {
        return (
            <div className="login">
                <h1>Login</h1>
                <div className="container">
                    <span>Username</span>
                    <input type="text" name="username" value={this.state.username} onChange={this.handleChange}/>
                    <br/>
                    <span>Password</span>
                    <input type="password" name="password" value={this.state.password} onChange={this.handleChange}/>
                    <br/>
                    <button className="btn btn-primary" onClick={this.loginClicked}>Login</button>
                    {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid credentials.</div>}
                    {this.state.showSuccessMessage && <div className="alert alert-success">Valid credentials.</div>}
                </div>
            </div>
        )
    }

    handleChange(event) {
        this.setState(
             {
                [event.target.name]: event.target.value
             }
        )
    }

    loginClicked() {
/*        if (this.state.username === 'sekvan' && this.state.password === 'sekvan') {
            AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
            this.props.history.push(`/welcome/${this.state.username}`);
        } else {
            this.setState(
                {
                    hasLoginFailed: true,
                    showSuccessMessage: false
                }
            )
        }*/

        // Basic Auth

/*        AuthenticationService.eecuteBasicAuthenticationService(this.state.username, this.state.password)
            .then(response => {
                AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
                this.props.history.push(`/welcome/${this.state.username}`);
            })
            .catch(error => {
                this.setState(
                    {
                        hasLoginFailed: true,
                        showSuccessMessage: false
                    }
                )
            })*/

        // JWT Auth

        AuthenticationService.executeJwtAuthenticationService(this.state.username, this.state.password)
            .then(response => {
                AuthenticationService.registerSuccessfulLoginWithJWTToken(this.state.username, response.data.token);
                this.props.history.push(`/welcome/${this.state.username}`);
            })
            .catch(error => {
                this.setState(
                    {
                        hasLoginFailed: true,
                        showSuccessMessage: false
                    }
                )
            })
    }

}

/*function ShowInvalidCredentials(props) {
    if (props.hasLoginFailed) {
        return <div>Invalid credentials.</div>
    }
    return null;
}

function ShowLoginSuccessMessage(props) {
    if (props.showSuccessMessage) {
        return <div>Valid credentials.</div>
    }
    return null;
}*/
