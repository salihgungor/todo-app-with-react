import React, {Component} from "react";
import {Link, withRouter} from "react-router-dom";
import AuthenticationService from "../authentication/AuthenticationService";

class HeaderComponent extends Component {

    render() {
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div>
                        <a href="/welcome" className="navbar-brand">My Page</a>
                    </div>
                    <ul className="navbar-nav">
                        {
                            isUserLoggedIn &&
                            <li className="nav-link">
                                <Link to="/welcome/sekvan" className="nav-link">Home</Link>
                            </li>
                        }
                        {
                            isUserLoggedIn &&
                            <li className="nav-link">
                                <Link to="/todos" className="nav-link">Todos</Link>
                            </li>
                        }
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        {
                            !isUserLoggedIn &&
                            <li className="nav-link">
                                <Link to="/login" className="nav-link">Login</Link>
                            </li>
                        }
                        {
                            isUserLoggedIn &&
                            <li className="nav-link">
                                <Link to="/logout" className="nav-link" onClick={AuthenticationService.logout}>Logout</Link>
                            </li>
                        }
                    </ul>
                </nav>
            </header>
        );
    }
}

export default withRouter(HeaderComponent);
