import './App.css';
import './bootstrap.css';
import React, {Component} from "react";
import LoginComponent from "./components/authentication/login/Login";
import WelcomeComponent from "./components/welcome/Welcome";

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import ErrorComponent from "./components/error/Error";
import ListTodosComponent from "./components/list-todos/ListTodos";
import HeaderComponent from "./components/header/Header";
import FooterComponent from "./components/footer/Footer";
import LogoutComponent from "./components/authentication/logout/Logout";
import AuthenticatedRouteComponent from "./components/authentication/authenticated-route/AuthenticatedRoute";
import TodoComponent from "./components/todo/TodoComponent";

class App extends Component {

  render() {
    return (
        <div className="App">
            <Router>
                <HeaderComponent></HeaderComponent>
                <Switch>
                    <Route path="/" exact component={LoginComponent}></Route>
                    <Route path="/login" component={LoginComponent}></Route>
                    <AuthenticatedRouteComponent path="/welcome/:name" component={WelcomeComponent}></AuthenticatedRouteComponent>
                    <AuthenticatedRouteComponent path="/todos/:id" component={TodoComponent}></AuthenticatedRouteComponent>
                    <AuthenticatedRouteComponent path="/todos" component={ListTodosComponent}></AuthenticatedRouteComponent>
                    <AuthenticatedRouteComponent path="/logout" component={LogoutComponent}></AuthenticatedRouteComponent>
                    <Route component={ErrorComponent}></Route>
                </Switch>
                <FooterComponent></FooterComponent>
            </Router>
        </div>
    )
  }

}

/*function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}*/

export default App;
